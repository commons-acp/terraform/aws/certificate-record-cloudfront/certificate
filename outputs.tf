output "domain_validation_options_resource_record_name" {
  value = aws_acm_certificate.cert_app.domain_validation_options.0.resource_record_name
}

output "domain_validation_options_domain_name" {
  value = aws_acm_certificate.cert_app.domain_validation_options.0.domain_name
}

output "domain_validation_options_resource_record_value" {
  value = aws_acm_certificate.cert_app.domain_validation_options.0.resource_record_value
}

output "domain_validation_options_resource_record_type" {
  value = aws_acm_certificate.cert_app.domain_validation_options.0.resource_record_type
}

output "domain_validation_options" {
  value = aws_acm_certificate.cert_app.domain_validation_options
}

output "cert_arn" {
  value = aws_acm_certificate.cert_app.arn
}