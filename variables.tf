
variable "fqdn" {
  type        = string
  description = "The FQDN of the website and also name of the S3 bucket"
}

